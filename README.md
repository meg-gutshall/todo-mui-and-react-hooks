This was made with Create React App as a Base, initially with typescript (moved back to jsx for time) and the mui (formerly material ui) framework.

With this I am able to:

* Add and view a task
* Delete a task
* Mark a task as completed
* Give a todo item an priority
* View the tasks sorted by priority and name (todo: add filterable callback)
* View the number of total and completed tasks
* Tests for TodoListItem


I used yarn 2, but you can use npm if you want as well.

Testing:
yarn test

Run development:
yarn start

Build production:
yarn build