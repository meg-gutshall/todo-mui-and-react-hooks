# Diffed Solution

See this [solution branch](https://gitlab.com/SuperRoach/todo-mui-and-react-hooks/-/merge_requests/1/diffs) by Brett (SuperRoach) of Party Corgi who original shared this take home challenge with me.

Showing how a managed branch can produce a cleaner codebase, and consistent work between team members.

Typically what you would do is sit down and agree on a coding style, editing the eslint.rc and coding conventions to what you'd prefer.
Or an organisation wide one could be made, that is the default for projects. It would allow for faster switching between projects.
