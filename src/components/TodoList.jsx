import List from '@mui/material/List';
import Grid from '@mui/material/Grid';
import TodoListItem from './TodoListItem';

/**
 * Todo List display
 * @constructor
 * @param {Array} items - Complete list of objects to display
 * @param {Array} filteredParams - accepts sortName, sortPriorityOnly
 * @param {Object} props - props to pass down.
 * @function onItemRemove - called by onDeleteClick to pass up item deletion
 * @function onItemComplete - called by onCheckBoxToggle to toggle completion status of a todo.
 * @function onItemPrioritise - called by onPrioritiseToggle to change the star/priority.
 * 
 */
function TodoList({ onItemComplete, onItemRemove, onItemPrioritise, items = [], ...props }) {
  return (
    <>
      {items && items.length > 0 && (
        <Grid container spacing={2}>
          <Grid item xs={12} md={10} sx={{ mx: "auto" }}>
            <List
              sx={{ width: '100%', bgcolor: 'background.paper' }}
            >
              {items.map((todo, index) => (
                <TodoListItem
                  // By using a spread operator here, I don't need to duplicate writing the
                  // booleans being passed through (checked, priority)
                  {...todo}
                  key={`TodoItem-${index}`}
                  // No divider for the last one
                  divider={index !== items.length - 1}
                  onCheckBoxToggle={() => onItemComplete(index)}
                  onPrioritiseToggle={() => onItemPrioritise(index)}
                  onDeleteClick={() => onItemRemove(index)}
                />
              ))}
            </List>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default TodoList;