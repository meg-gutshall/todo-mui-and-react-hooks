import { render, screen } from '@testing-library/react';
import TodoListItem from './TodoListItem';

test('Given text is displayed in a list item', () => {
  render(<TodoListItem text="test text displays here" />);
  const ListItemText = screen.getByText(/test text displays here/i);
  expect(ListItemText).toBeInTheDocument();
});

test('A low priority item has a hollow star icon', () => {
  render(<TodoListItem text="Star test." priority={false} />);
  const StarIcon = screen.getByTestId('StarBorderOutlinedIcon')
  expect(StarIcon).toBeInTheDocument();
});

test('A high priority item has a solid star icon', () => {
  render(<TodoListItem text="Star test." priority={true} />);
  const StarIcon = screen.getByTestId('StarOutlinedIcon')
  expect(StarIcon).toBeInTheDocument();
});
